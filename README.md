#NTT-ME採用サイトのレポジトリです。

# Git操作 #
Backlog側でプルリクとマージが出来ないので、下記のようなgit操作で代替します。
田中以外はmasterブランチに手を付けずpullのみを行い、各々のブランチのみで作業を続ける、という手順です。

作業者操作
作業者のローカルで各作業ブランチ(命名規則なし)ブランチ作成
ブランチを各作業ブランチに切り替え
ファイルの作成、修正
add、commit ←【1】
各作業ブランチをBacklogリポにpush

田中操作
各作業ブランチをpull後、確認、修正依頼、完了
任意のブランチ名をmasterにmerge
merge後のmasterをBacklogリポにpush

作業者の操作
Backlogでmerge後のmasterを確認
ブランチをmasterに切り替え
pull
ブランチを各作業ブランチに切り替え
masterブランチを各作業ブランチにmerge(各作業ブランチで作業中のファイルはadd、commitしていない前提)
各作業ブランチで作業を続ける
【1】へ。

# git push でサーバーに公開する設定方法 #
http://qiita.com/kogai/items/cd9d0254e19bcb1f653b

# サーバの情報 #
/web/astrakhan-test.com/ntt-me/

ftp165.heteml.jp

astrakhan

%%astweb246%%
magonote246

## リモートレポジトリURL ##
ssh://astrakhan@ssh165.heteml.jp:2222/home/sites/heteml/users/a/s/t/astrakhan/web/astrakhan-test.com/ntt-me/site.git

## リモートレポジトリパスワード ##
magonote246