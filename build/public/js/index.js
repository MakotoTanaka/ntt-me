$(window).load(function(){
	imgToCss();
});

function imgToCss(){
	var imgSrc = $("img").attr("src");
	var imgSrcHeight = $("img").height();
	$('a').css({
	    'background-image':'url('+imgSrc+')',
	    'height':imgSrcHeight+'px',
	});
	$('img').hide();
}
