<footer>
	<div class="wrap">
		<h2><img src="/recruit/2016/common/img/footer-logo.jpg" alt="MTTME ネットワーク総合エンジニアリング企業"></h2>
		<h3><a href="/recruit/2016/">HOME</a></h3>
		<ul>
			<li>
				<h3><a href="/recruit/2016/works/works.html">WORKS</a></h3>
				<ul>
					<li><a href="/recruit/2016/works/works.html">私たちの仕事</a></li>
					<li><a href="/recruit/2016/works/universal.html">すべての人がつながるために</a></li>
				</ul>
			</li>
			<li>
				<h3><a href="/recruit/2016/engineers/senior/index.html">ENGINEERS</a></h3>
				<ul>
					<li><a href="/recruit/2016/engineers/senior/index.html">先輩たちの仕事紹介 </a></li>
					<li><a href="/recruit/2016/engineers/zyoshi.html">ME女子社員対談</a></li>
					<li><a href="/recruit/2016/engineers/dna01.html">つなぐDNA対談　アクセス系編</a></li>
					<li><a href="/recruit/2016/engineers/dna02.html">つなぐDNA対談　ネットワーク系編</a></li>
					<li><a href="/recruit/2016/engineers/100/index.html">100人のエンジニア</a></li>
				</ul>
			</li>
			<li>
				<h3><a href="/recruit/2016/company/company.html">COMPANY</a></h3>
				<ul>
					<li><a href="/recruit/2016/company/company.html">会社概要</a></li>
					<li><a href="/recruit/2016/company/train/index.html">育成・研修体系　キャリアプラン</a></li>
					<li><a href="/recruit/2016/company/change/index.html">情報通信は世の中を変える</a></li>
				</ul>
			</li>
			<li>
				<h3><a href="/recruit/2016/recruit/charge.html">RECRUIT</a></h3>
				<ul>
					<li><a href="/recruit/2016/recruit/charge.html">採用責任者の声</a></li>
					<li><a href="/recruit/2016/recruit/point.html">募集要項</a></li>
					<li><a href="/recruit/2016/recruit/schedule.html">新卒採用スケジュール</a></li>
					<li><a href="/recruit/2016/recruit/public.html">福利厚生＆休暇等</a></li>
					<li><a href="/recruit/2016/recruit/faq.html">FAQ</a></li>
				</ul>
			</li>
			<li><h3><a href="/recruit/2016/quiz/index.html">NTT-ME クイズ</a></h3></li>
		</ul>
	</div>
	<p id="copyright">Copyright ©2015 NTT ME CORPORATION All rights reserved.</p>
</footer>