<header>
	<div class="header-top">
		<div class="wrap">
			<h1><a href="/recruit/2016/" class="hover"><img src="/recruit/2016/common/img/logo.jpg" alt="NTT-ME 2016 RECRUITING SITE"></a></h1>
			<ul>
				<li><a href="https://job.axol.jp/16/s/nttme/mypage/" class="hover" target="_blank"><img src="/recruit/2016/common/img/menu-btn1.jpg" alt="マイページ"></a></li>
				<li><a href="/recruit/2016/pre/index.html" class="hover" target="_blank"><img src="/recruit/2016/common/img/menu-btn2.jpg" alt="プレエントリー"></a></li>
			</ul>
		</div>
	</div>