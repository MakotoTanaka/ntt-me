$(function(){
	var winW = window.innerWidth ? window.innerWidth: $(window).width();
	var winH = window.innerHeight ? window.innerHeight: $(window).height();

	$(window).resize(function(){
		winW = window.innerWidth ? window.innerWidth: $(window).width();
		winH = window.innerHeight ? window.innerHeight: $(window).height();
	});

	//スマフォ判定
	var $sp = false;
	if((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || 
		navigator.userAgent.indexOf('iPod') > 0 || 
		navigator.userAgent.indexOf('Android') > 0
	){
		$sp = true;
	}else{
		$(".pc-none").css({display: "none"});
	}


	/*=========================================
	 * ページ遷移
	 *=======================================*/
	$("a.scroll").on("click", function(e){
		e.preventDefault();
		var href = $(this).attr("href");
		var pos = $($(this).attr("href")).offset().top - 46;
		$(/safari/i.test(navigator.userAgent) ? 'body' : 'html').stop().animate({ scrollTop:pos },{ duration: 600, easing: "swing" });
		return false;
	});

	/*=========================================
	 * ヘッダー、サブメニュー選択
	 *=======================================*/
	if($('[data-header]').length > 0){
		var headtype = $('body').data('header');
		var subtype =  parseInt($('body').data('sub-header')) - 1;
		switch(headtype){
		case 'works':
			$(".header-btm ul li").eq(0).addClass("slc");
			$(".sub-menu").eq(0).addClass("slc").find("li").eq(subtype).addClass('slc');
			break;
		case 'engineers':
			$(".header-btm ul li").eq(1).addClass("slc");
			$(".sub-menu").eq(1).addClass("slc").find("li").eq(subtype).addClass('slc');
			break;
		case 'company':
			$(".header-btm ul li").eq(2).addClass("slc");
			$(".sub-menu").eq(2).addClass("slc").find("li").eq(subtype).addClass('slc');
			break;
		case 'recruit':
			$(".header-btm ul li").eq(3).addClass("slc");
			$(".sub-menu").eq(3).addClass("slc").find("li").eq(subtype).addClass('slc');
			break;
		}
	}
	/************************************************************************
	 * TOPページ
	 ***********************************************************************/
	if($("#top-page").length > 0){
		$('#keyimg').bxSlider({
			mode: 'fade',
			auto: true,
			speed: 1000,
			pause: 4000,
			autoHover: true
		});

		setTimeout(function(){
			$("#top-message").height($('body,html').height());
		},1000);

		$("#message-open").click(function(){
			$("#top-message").addClass("open");
		});
		$("#message-close").click(function(){
			$("#top-message").removeClass("open");
		});


		/*=========================================
		 * ニュースフィード
		 *=======================================*/
		jQuery.ajax({
			url:'/recruit/2016/news/news.xml',
			type:'GET',
			timeout:30000,
			dataType: "xml",
			error:function(){
	//			alert("Error")
			},
			success: function(data){
				var $feed = $(data);
				var $entries = $feed.find("item");
				var $target = $(".news-box ul.box");
				$target.html("");
				$entries.each(function(i, ele){

					var $ele = $(ele);
					var li = $('<li></li>');
					if((i % 2) != 0){
						li = $('<li class="bg"></li>');
					}
					var anc = $('<a href="" target="" class="hover"></a>');
					var time = $('<span class="date"></span>');
					var title = $('<span class="txt"></span>')
					var date_str = String($ele.find("day").text());
					var split = date_str.split(".");
					var pdate = new Date(split[0] , split[1]-1 , split[2]);
					var pday = pdate.getDate();
					var pmonth = pdate.getMonth() + 1;
					var pyear = pdate.getFullYear();
					var url = $ele.find("url").text();

					time.append(pyear + "." + day_format(pmonth) + "." + day_format(pday) + " ");
					title.append($ele.find("content").text());
					anc.attr("href", $ele.find("url").text());
					anc.attr("target", $ele.find("target").text());

					if(url.length == 0){
						anc = $('<p></p>');
					}

					anc.append(time);
					if( isNewArticle(pdate) ){
						anc.append('<img src="./images/new.jpg" alt="NEW">');
					};
					anc.append(title);
					li.append(anc);
					$target.append(li);
				});

				$(".news-box .box").tile(2);
			}
		});

		/* -----------------------------------------------
		 * 直近か判定
		 * ----------------------------------------------- */
		function isNewArticle(dateTmp){
			var limit = 3;	//●日前のニュースまでをNEWと判定
			var dateNow = new Date();
			dateNow = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(),dateNow.getHours(), dateNow.getMinutes(), dateNow.getMilliseconds());
			return ((dateNow.getTime() - dateTmp.getTime()) > (limit * 24 * 60 * 60 * 1000))?false:true;
		}

		function day_format($num){
			if(String($num).length == 1){
				return "0" + String($num);
			}else{
				return $num;
			}
		}
	}
	/************************************************************************
	 * 私たちの仕事
	 ***********************************************************************/
	if($(".tab-box").length > 0){
		$(".tab-box .tab").click(function(){
			$(".tab-box .tab").removeClass('on');
			$(".tab-box .box").removeClass('on');
			$(this).addClass("on");
			$(this).next().addClass("on");
		});
	}

	/************************************************************************
	 * youtubeモーダル
	 ***********************************************************************/
	if($(".modal-youtube").length > 0){
		//fancybox設定
		$(".modal-youtube").fancybox({
			margin: 0,
			padding: 0,
			width: 854,
			height: 480,
			overlayColor: '#330000',
			overlayOpacity: '0.6',
			type: 'iframe'
		});

		if($sp){
			$(".modal-youtube").click(function(e){
				$("#fancybox-overlay").width(985);
				setTimeout(function(){
					$("#fancybox-wrap").width(854);
					$("#fancybox-wrap").css({left:"65px"});
					$("#fancybox-content,#fancybox-wrap iframe").width(854);
				},1000);
			});
		}
	}

	/************************************************************************
	 * すべての人がつながるために
	 ***********************************************************************/
	if($(".universal-modal").length > 0){
		//fancybox設定
		$(".universal-modal").fancybox({
			margin: 0,
			padding: 0,
			width: 600,
			height: 500,
			overlayColor: '#330000',
			overlayOpacity: '0.6',
			type: 'iframe'
		});

		if($sp){
			$(".universal-modal").click(function(e){
				$("#fancybox-overlay").width(985);
				setTimeout(function(){
					$("#fancybox-wrap").width(600);
					$("#fancybox-wrap").css({left:"192px"});
					$("#fancybox-content,#fancybox-wrap iframe").width(600);
				},1000);
			});
		}
	}

	/************************************************************************
	 * 100人のエンジニアページ
	 ***********************************************************************/
	if($("#hundred-box").length > 0){
		var $hcontent	= $("#hundred-box"),
			$hbox		= $("#hundred-box .box"),
			$hdetai		= $("#hundred-detail"),
			$job		= {1:'アクセス系業務',2: 'ネットワーク系業務'};
		var $classJ		= {1:'access',2: 'network'};

		var $classC 	= {	'情報系':'c1',
							'電気系':'c2',
							'その他':'c3'};

		var $classQ		= {	'ＭＥの自慢、聞かせてください！':'q1',
							'うれしかったエピソード':'q2',
							'こんな人が向いている':'q3',
							'こんな後輩と一緒にはたらきたい！':'q4',
							'入社の決め手は？':'q5',
							'私の仕事':'q6',
							'学生時代とかわったことは？':'q7',
							'学生時代の勉強・経験で役立ったこと':'q8',
							'後輩のみなさんへのメッセージ':'q9',
							'今までにどんな資格をとりましたか':'q10',
							'今後の目標':'q11',
							'常に心がけていることは？':'q12',
							'職場はどんなところですか？':'q13',
							'成長を感じたこと':'q14',
							'入社して、イメージと違っていたことは？':'q15',
							'入社してよかったと思うことは？':'q16'};

		var $optionCheck = $(".option-check a"),
			$optionSelect = $('#hundred-options .select-box>a'),
			$optionSelectLinks = $('#hundred-options .option-select a');

		function setCsvData(data){

				// Convert to Unicode.
				var unicode = Encoding.convert(data,{
					to: 'UNICODE',
					from: 'AUTO'
				});
				data = Encoding.codeToString(unicode);

				var obj = $.csv()(data);
				var arrTitle = obj.shift();
				obj = arrShuffle(obj);
				
				//100エンジニアhtml設置
				var $html = "";
				$.each(obj,function(key,value){
					$html += '<div class="box '+$classJ[value[1]]+' '+value[3]+' '+$classC[value[4]]+' '+$classQ[value[5]]+'" data-thumb="'+value[8]+'" data-job="'+$classJ[value[1]]+'" onclick="'+ "ga('send', 'event', '100人のエンジニア', 'クリック', '"+ value[0] +"_" +value[2]+"');"+ '">' +
							'	<img src="./member/t/'+value[7]+'" class="hover">' +
							'	<p class="job">'+$job[value[1]]+'</p>' +
							'	<p class="name">'+value[2]+'</p>' +
							'	<p class="year">'+value[3]+'</p>' +
							'	<p class="course">'+value[4]+'</p>' +
							'	<p class="quest">'+value[5]+'</p>' +
							'	<p class="answer">'+value[6]+'</p>' +
							'</div>';

				});

				$hcontent.append($html);
				$hbox = $("#hundred-box .box");
				setIsotope();
		}

		function setIsotope(){
			//オプション表示変更
			$optionCheck.click(function(e){
				if($(this).hasClass("selected")){
					$(this).removeClass("selected")
				}else{
					$(this).addClass("selected")
				}
				sortIsotope();
			});
			//セレクトボックス表示、非表示
			$optionSelect.click(function(e){
				e.preventDefault();
				$(this).next().slideToggle();
			});
			//セレクトボックス選択
			$optionSelectLinks.click(function(e){
				e.preventDefault();
				var $parent = $(this).parent().parent();
				$parent.find("a").removeClass('selected')
				$(this).addClass("selected");
				$parent.hide();
				$parent.prev().text($(this).text());
				sortIsotope();
			});

			//詳細表示
			$hbox.click(function(e){
				//情報設置
				$hdetai.find(".job").html('<span class="'+$(this).data('job')+'">'+$(this).find('.job').text()+'</span>');
				$hdetai.find(".thumb").html('<img src="./member/m/'+$(this).data('thumb')+'" alt="'+$(this).find('.name').text()+'">');
				$hdetai.find("h2").html('<em>'+$(this).find('.name').text()+'</em>（'+$(this).find('.year').text()+'年入社）');
				$hdetai.find(".work").html($(this).find('.work').text());
				$hdetai.find(".course").html($(this).find('.course').text());
				$hdetai.find("h3").html($(this).find('.quest').text());
				$hdetai.find(".answer").html($(this).find('.answer').text());

				if($hdetai.css('display') != 'block') {
					$hdetai.slideToggle();
				}

				var pos = $hdetai.offset().top - 46;
				$(/safari/i.test(navigator.userAgent) ? 'body' : 'html').stop().animate({ scrollTop:pos },{ duration: 600, easing: "swing" });
			});

			//プラグイン設置初期
			$hcontent.isotope({
				itemSelector : '.box'
			});
			randomLayout();
			$hcontent.isotope();
		}

		//選択表示変更
		function sortIsotope(){
			var arr = [];
			if($(".check-value.selected").length > 0){
				$(".check-value").each(function(key,value){
					if(!$(value).hasClass('selected')){
						arr.push($(value).data("option-value"));
					}
				});
			}
			$(".select-value").each(function(key,value){
				if($(value).hasClass('selected')){
					arr.push($(value).data("option-value"));
				}
			});
			var value = arr.join('');
			var options = {};
			options['filter'] = value;

			// otherwise, apply new options
			$hcontent.isotope(options);
			randomLayout()
			$hcontent.isotope(options);
//			$hcontent.isotope("shuffle");
		}

		function randomLayout(){
			$hbox.removeClass("type");
			var count = $('.isotope-item:not(.isotope-hidden)').length;
			var nMin = 1,
				nMax = 6;
			var int = Math.floor(Math.random()*(nMax-nMin+1))+nMin;

			var nMin2 = 5,
				nMax2 = 6;
			var int2 = Math.floor(Math.random()*(nMax2-nMin2+1))+nMin2;
			
			for(var i=int; i < count;){
				$('.isotope-item:not(.isotope-hidden)').eq(i).addClass("type");
				i += int2;
			}
		}

		binaryAjax('GET' , '/recruit/2016/engineers/100/csv/100.csv' , null , setCsvData);
	}

	/************************************************************************
	 * NTT-ME クイズ
	 ***********************************************************************/
	if($(".quiz-quest-btn").length >0){
		var $onimg = new Array();
		var $img = new Array();
		$(".quiz-quest-btn img").each(function(key,value){
			var src = $(value).attr('src');
			var ftype = src.substring(src.lastIndexOf('.'), src.length);
			var hsrc = src.replace(ftype, '-on'+ftype);
			$onimg[key] = hsrc;
			$img[key] = src;
		});

		$(".quiz-quest-btn").click(function(e){
			e.preventDefault();

			$(".quiz-quest-btn img").each(function(key,value){
				$(value).attr('src',$img[key]);
			});

			var index = $(".quiz-quest-btn").index(this);

			$(this).find("img").attr('src',$onimg[index]);

			$(".quiz-answer li").css({display: "none"});
			if(!$(".quiz-answer").hasClass("open")){
				$(".quiz-answer li").eq(index).slideToggle();
				$(".quiz-answer").addClass("open");
			}else{
				$(".quiz-answer li").eq(index).css({display: "block"});
			}
		});
	}

	function arrShuffle(array) {
		var n = array.length, t, i;

		while (n) {
			i = Math.floor(Math.random() * n--);
			t = array[n];
			array[n] = array[i];
			array[i] = t;
		}

		return array;
	}
});