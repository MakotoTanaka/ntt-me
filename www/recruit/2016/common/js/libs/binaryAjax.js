// binary受信用のAjax.
// method : POST or GET.
// url : 接続先URL.
// params : パラメータ設定(Map定義 or 文字列).
// func : コールバックファンクション.
//        コールバックファンクションを設定しない場合は、同期取得(非推奨).
// 戻り値 : 同期取得の場合のみArray or ArrayBuffer系で返却される.
var binaryAjax = (function() {

// IEチェック.
var _isMSIE = ( /*@cc_on!@*/false ) ;
if( !_isMSIE ) {
    try {
        new XDomainRequest() ;
        _isMSIE = true ;
    } catch( e ) {
    }
}
// IEバージョン取得.
var _ieVer = ( function() {
    if( _isMSIE ) {
        if( typeof( document.documentElement.style.msInterpolationMode ) == "undefined" ) {
            return 6 ;
        }
        var array = /(msie|rv:?)\s?([\d\.]+)/.exec(window.navigator.userAgent.toLowerCase());
        return (array) ? array[2]|0 : -1 ;
    }
    return -1 ;
})() ;

// 古いIE version9以下の場合.
// 現在WebSocketを対応していない最新のブラウザは[IE9以下].
// (WindowsVistaがサポートする最終IEがバージョン9).
var oldIE = _isMSIE && ( _ieVer <= 9 ) ;

// 処理対象のAjaxオブジェクトを取得.
var _obj = (function() {
    
    // 古いIE(vistaIE9)の場合は、ActiveXObjectを利用する.
    if( oldIE ) {
        try {
            new ActiveXObject( "Msxml2.XMLHTTP.6.0" ) ;
            return function() {
                return new ActiveXObject( "Msxml2.XMLHTTP.6.0" ) ;
            }
        } catch( e ) {
        }
        try {
            new ActiveXObject( "Msxml2.XMLHTTP.3.0" ) ;
            return function() {
                return new ActiveXObject( "Msxml2.XMLHTTP.3.0" ) ;
            }
        } catch( e ) {
        }
        try {
            new ActiveXObject( "Msxml2.XMLHTTP" ) ;
            return function() {
                return new ActiveXObject( "Msxml2.XMLHTTP" ) ;
            }
        } catch( e ) {
        }
        return function() {
            return new ActiveXObject( "Microsoft.XMLHTTP" ) ;
        }
    }
    // 最新のブラウザの場合.
    else {
        return function() {
            return new XMLHttpRequest() ;
        } ;
    }
})() ;

// バイナリAjax送信.
var _bajx = (function() {
    
    // GETの場合のパラメータセット.
    var _getToUrlParam = function( u,p ) {
        if( u.indexOf( "?" ) != -1 ) {
            return u + "&" + p ;
        }
        return u + "?" + p ;
    }
    
    // postヘッダセット.
    var _toPostHeader = function( ajx ) {
        if( ajx.setRequestHeader ) {
            ajx.setRequestHeader('Content-type','application/x-www-form-urlencoded') ;
        }
    }
    
    // バイナリ用ヘッダセット.
    var _binHeader = function( ajx ) {
        if( ajx.setRequestHeader ) {
//            ajx.setRequestHeader( "Accept-Charset","x-user-defined" ) ;
        }
        if( ajx.overrideMimeType ) {
            ajx.overrideMimeType( 'text/plain; charset=x-user-defined' ) ;
        }
    }
    
    // 古いIE(vistaIE9)の場合.
    if( oldIE ) {
        
        return (function() {
            
            // [IE]Ajax.responseBodyの内容を配列変換する.
            var _ie9binAjaxInitFlg = false ;
            var _oldIeRcvBin = function( binary ) {
                if( !_ie9binAjaxInitFlg ) {
                    var script = document.createElement( "script" ) ;
                    script.setAttribute( "type","text/vbscript" ) ;
                    script.text =
                        "Function vbs_len(b)vbs_len=LenB(b)End Function\n" +
                        "Function vbs_str(b)vbs_str=CStr(b)+chr(0)End Function\n" ;
                    document.getElementsByTagName("head")[0].appendChild( script ) ;
                    _ie9binAjaxInitFlg = true ;
                }
                var data = vbs_str( binary ) ;
                var len = vbs_len( binary ) ;
                if( len == 0 ) return null ;
                var n,c,iLen ;
                var ret = new Array( len ) ;
                c = 0 ;
                iLen = len >> 1 ;
                for( var i = 0 ; i < iLen ; i ++ ) {
                    n = data.charCodeAt(i) ;
                    ret[ c++ ] = n&255 ;
                    ret[ c++ ] = (n>>8)&255 ;
                }
                if( len & 1 == 1 ) {
                    ret[ c ] = data.charCodeAt( i )&255 ;
                }
                return ret ;
            }
            return function( ajx,method,url,params,func ) {
                var post = false ;
                if( ( method = method.toUpperCase() ) == "POST" ) {
                    post = true ;
                }
                else if( params != undefined && params != null && params.length > 0 ) {
                    url = _getToUrlParam( url,params ) ;
                    params = null ;
                }
                else {
                    params = null ;
                }
                // 非同期.
                if( typeof( func ) == "function" ) {
                    ajx.open(method,url,true) ;
                    ajx.onreadystatechange=function(){
                        if( ajx.readyState==4 ) {
                            func(_oldIeRcvBin(ajx.responseBody)) ;
                        }
                    } ;
                    ajx.send(params) ;
                }
                // 同期.
                else {
                    ajx.open(method,url,false) ;
                    ajx.send(params) ;
                    return _oldIeRcvBin(ajx.responseBody) ;
                }
            }
        })() ;
    }
    // 最新のブラウザの場合.
    else {
        
        return (function() {
            
            var _aryAjx2,_aryOldAjx ;
            var _ajx2Flg = false ;
            if( window["Uint8Array"] != undefined ) {
                _ajx2Flg = true ;
                _aryAjx2 = function( ajx ) {
                    var n = ajx.response ;
                    if( n == undefined || n == null || n.length == 0 ) {
                        return null ;
                    }
                    return new Uint8Array( n ) ;
                }
                _aryOldAjx = function( ajx ) {
                    var n = ajx.responseText ;
                    if( n == undefined || n == null || n.length == 0 ) {
                        return null ;
                    }
                    var len = n.length ;
                    var ret = new Uint8Array( len ) ;
                    for( var i = 0 ; i < len ; i ++ ) {
                        ret[ i ] = n.charCodeAt( i ) & 0xff ;
                    }
                    return ret ;
                }
            }
            else {
                _aryAjx2 = function( ajx ) {
                    var n = ajx.responseText ;
                    if( n == undefined || n == null || n.length == 0 ) {
                        return null ;
                    }
                    var len = n.length ;
                    var ret = new Array( len ) ;
                    for( var i = 0 ; i < len ; i ++ ) {
                        ret[ i ] = n.charCodeAt( i ) & 0xff ;
                    }
                    return ret ;
                }
                _aryOldAjx = _ary ;
            }
            return function( ajx,method,url,params,func ) {
                var post = false ;
                if( (method = method.toUpperCase()) == "POST" ) {
                    post = true ;
                }
                else if( params != undefined && params != null && params.length > 0 ) {
                    url = _getToUrlParam( url,params ) ;
                    params = null ;
                }
                else {
                    params = null ;
                }
                // 非同期の場合は、利用可能な場合は[arrayBuffer]を利用.
                if( typeof( func ) == "function" ) {
                    ajx.open(method,url,true) ;
                    if( _ajx2Flg ) {
                        ajx.responseType = "arraybuffer" ;
                        ajx.onload=function(){
                            func( _aryAjx2(ajx) ) ;
                        } ;
                    }
                    else {
                        ajx.onreadystatechange=function(){
                            if( ajx.readyState==4 ) {
                                func( _aryOldAjx(ajx) ) ;
                            }
                        } ;
                    }
                    _binHeader( ajx ) ;
                    if( post ) {
                        _toPostHeader( ajx ) ;
                    }
                    ajx.send(params) ;
                }
                // 同期の場合は[arraybuffer]はIE10以降だけに適用.
                else {
                    ajx.open(method,url,false) ;
                    if( _ajx2Flg && _isMSIE ) {
                        ajx.responseType = "arraybuffer" ;
                    }
                    _binHeader( ajx ) ;
                    if( post ) {
                        _toPostHeader( ajx ) ;
                    }
                    ajx.send(params) ;
                    return ( _ajx2Flg && _isMSIE ) ?
                        _aryAjx2(ajx) :
                        _aryOldAjx(ajx) ;
                }
            }
        })() ;
    }
})() ;

// 実行メソッド.
return function( method,url,params,func ) {
    
    url += (( url.indexOf( "?" ) == -1 )? "?":"&" )+(new Date().getTime()) ;
    var pms = null ;
    if( params != null && params != undefined ) {
        if( typeof( params ) == "string" ) {
            pms = params ;
        }
        else {
            pms = "" ;
            for( var k in params ) {
                pms += "&" + k + "=" + encodeURIComponent( params[ k ] ) ;
            }
        }
    }
    return _bajx( _obj(),method,url,pms,func ) ;
}

})() ;