$(function(){
	var $winW = window.innerWidth ? window.innerWidth : $(window).width();
	var $winH = window.innerHeight ? window.innerHeight : $(window).height();
	var $menuM = 37;
	var $wrapper	= $("#wrapper"),
		$header		= $("header"),
		$gnavibtn	= $("#gnavi-btn"),
		$gnavi		= $("#gnavi"),
		$gnaviarea	= $("#gnavi nav"),
		$container	= $("#container");

	/*=========================================
	 * androidバージョン判別
	 * @param parseFloat n	n以上の場合：true
	 *=======================================*/
	function Androidversion(n) {
		var bo = false;
		var ua = navigator.userAgent.toLowerCase();
		var version = ua.substr(ua.indexOf('android')+8, 3);
		if(ua.indexOf("android")) if(n <= parseFloat(version)) bo = true;
		return bo;
	}
	/*=========================================
	 * iOSバージョン判別
	 * @param parseFloat n	n以上の場合：true
	 *=======================================*/
	function iOSversion(n) {
		var bo = false;
		if (/iP(hone|od|ad)/.test(navigator.platform)) {  
			var v = (navigator.appVersion).match(/OS (\d+)_(\d+)_?(\d+)?/);
			if(n <= parseFloat([parseInt(v[1], 10) + "." + parseInt(v[2], 10) + "" + parseInt(v[3] || 0, 10)])) bo = true;
		}
		return bo;
	}
	/*=========================================
	 * ページ遷移
	 *=======================================*/
	$("a.scroll").on("click", function(e){
		e.preventDefault();
		var href = $(this).attr("href");
		var pos = $($(this).attr("href")).offset().top - 40;
		$(/safari/i.test(navigator.userAgent) ? 'body' : 'html').stop().animate({ scrollTop:pos },{ duration: 600, easing: "swing" });
		return false;
	});

	/*=========================================
	 * ウィンドウリサイズ処理
	 *=======================================*/
	function windowResize(){
		$winW = window.innerWidth ? window.innerWidth : $(window).width();
		$winH = window.innerHeight ? window.innerHeight : $(window).height();

		//グローバルナビサイズ調整
		$gnaviarea.width($winW-$menuM);
		$gnaviarea.height($winH);
		//全体高さ調整
		$wrapper.width($winW);

	}
	/************************************************************************
	 * TOPページ
	 ***********************************************************************/
	if($("#keyimg").length > 0){
		$('#keyimg').bxSlider({
			auto: true,
			speed: 1000,
			pause: 4000,
			autoHover: true
		});

		/*=========================================
		 * ニュースフィード
		 *=======================================*/
		jQuery.ajax({
			url:'/recruit/2016/news/news.xml',
			type:'GET',
			timeout:30000,
			dataType: "xml",
			error:function(){
	//			alert("Error")
			},
			success: function(data){
				var $feed = $(data);
				var $entries = $feed.find("item");
				var $target = $(".news-box ul");
				$target.html("");
				$entries.each(function(i, ele){

					var $ele = $(ele);
					var li = $('<li></li>');
					var anc = $('<a href="" target="" class="hover"></a>');
					var time = $('<span class="date"></span>');
					var title = $('<span class="txt"></span>')
					var date_str = String($ele.find("day").text());
					var split = date_str.split(".");
					var pdate = new Date(split[0] , split[1]-1 , split[2]);
					var pday = pdate.getDate();
					var pmonth = pdate.getMonth() + 1;
					var pyear = pdate.getFullYear();
					var url = $ele.find("url").text();

					time.append(pyear + "." + day_format(pmonth) + "." + day_format(pday) + " ");
					title.append($ele.find("content").text());
					anc.attr("href", $ele.find("url").text());
					anc.attr("target", $ele.find("target").text());

					if(url.length == 0){
						anc = $('<p></p>');
					}
					
					anc.append(time);
					if( isNewArticle(pdate) ){
						anc.append('<img src="./images/new.jpg" alt="NEW">');
					};
					anc.append(title);
					li.append(anc);
					$target.append(li);
				});
			}
		});

		/* -----------------------------------------------
		 * 直近か判定
		 * ----------------------------------------------- */
		function isNewArticle(dateTmp){
			var limit = 3;	//●日前のニュースまでをNEWと判定
			var dateNow = new Date();
			dateNow = new Date(dateNow.getFullYear(), dateNow.getMonth(), dateNow.getDate(),dateNow.getHours(), dateNow.getMinutes(), dateNow.getMilliseconds());
			return ((dateNow.getTime() - dateTmp.getTime()) > (limit * 24 * 60 * 60 * 1000))?false:true;
		}

		function day_format($num){
			if(String($num).length == 1){
				return "0" + String($num);
			}else{
				return $num;
			}
		}
	}
	/************************************************************************
	 * モーダル
	 ***********************************************************************/
	if($(".modal-window").length > 0){
		//fancybox設定
		$(".modal-window").fancybox({
			margin: 0,
			padding: 0,
			width: ($winW - 26),
			height: 330,
			overlayColor: '#330000',
			overlayOpacity: '0.6',
			type: 'iframe'
		});
	}

	/************************************************************************
	 * 初期処理開始
	 ***********************************************************************/
	//iOSバージョン6以上、androidバージョン4以上の場合
	if(iOSversion(6) || Androidversion(4)){
		//ヘッダー固定
		$gnavi.css({position: "fixed"});
		$header.css({position: "fixed"});
		$gnaviarea.css("-webkit-overflow-scrolling","touch");
	}

	$(".toggle").bind("click" , function(e){
		e.preventDefault();
		$(this).next().slideToggle();
	});

	//グローバルナビ表示
	$gnavibtn.bind("click" , function(){
		windowResize();
		if($container.hasClass("open")){
			$container.removeClass("open");
			$header.stop().transition({x:0} , 500 );
			$container.stop().transition({x:0} , 500 , function(e){
				$gnavi.css({display: "none"});
			});
		}else{
			$container.addClass("open");
			$gnavi.css({display: "block"});
			$header.stop().transition({x:$winW-$menuM} , 500);
			$container.stop().transition({x:$winW-$menuM} , 500);
		}
	})

	/*リサイズ処理*/
	$(window).bind("resize" , windowResize);
	$(window).bind("scroll" , windowResize);
	windowResize();
});