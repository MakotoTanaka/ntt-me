<header>
	<div class="header-top">
		<div class="wrap">
			<h1><a href="/recruit/2017/" class="hover"><img src="/recruit/2017/common/img/logo.jpg" alt="NTT-ME 2017 RECRUITING SITE"></a></h1>
			<ul>
				<li><a href="https://job.axol.jp/16/s/nttme/mypage/" class="hover" target="_blank"><img src="/recruit/2017/common/img/menu-btn1.jpg" alt="マイページ"></a></li>
				<li><a href="/recruit/2017/pre/index.html" class="hover" target="_blank"><img src="/recruit/2017/common/img/menu-btn2.jpg" alt="プレエントリー"></a></li>
			</ul>
		</div>
	</div>