<script type="text/javascript" src="/recruit/2017/common/js/libs/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/jquery.easing.min.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/jquery.tile.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/fancybox-b/jquery.fancybox-b-1.3.4.pack.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/jquery.isotope.min.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/jquery.csv.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/binaryAjax.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/scrolltopcontrol.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/libs/encoding.js"></script>
<script type="text/javascript" src="/recruit/2017/common/js/common.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-60070406-2', 'auto');
  ga('send', 'pageview');

</script>