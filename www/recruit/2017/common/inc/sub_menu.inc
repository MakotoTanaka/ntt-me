	<nav class="wrap">
		<ul class="sub-menu cfix">
			<li><a href="/recruit/2017/works/works.html" class="hover"><img src="/recruit/2017/common/img/works-menu1.png" alt="私たちの仕事"></a></li>
			<li><a href="/recruit/2017/works/universal.html" class="hover"><img src="/recruit/2017/common/img/works-menu2.png" alt="すべての人がつながるために"></a></li>
		</ul>
		<ul class="sub-menu cfix">
			<li><a href="/recruit/2017/engineers/senior/index.html" class="hover"><img src="/recruit/2017/common/img/engineers-menu1.png" alt="先輩たちの仕事紹介"></a></li>
			<li><a href="/recruit/2017/engineers/zyoshi.html" class="hover"><img src="/recruit/2017/common/img/engineers-menu2.png" alt="ME女子社員対談"></a></li>
			<li><a href="/recruit/2017/engineers/dna01.html" class="hover"><img src="/recruit/2017/common/img/engineers-menu3.png" alt="つなぐDNA対談　アクセス系編"></a></li>
			<li><a href="/recruit/2017/engineers/dna02.html" class="hover"><img src="/recruit/2017/common/img/engineers-menu4.png" alt="つなぐDNA対談　ネットワーク系編"></a></li>
			<li><a href="/recruit/2017/engineers/100/index.html" class="hover"><img src="/recruit/2017/common/img/engineers-menu5.png" alt="100人のエンジニア"></a></li>
		</ul>
		<ul class="sub-menu cfix">
			<li><a href="/recruit/2017/company/company.html" class="hover"><img src="/recruit/2017/common/img/company-menu2.png" alt="会社概要"></a></li>
			<li><a href="/recruit/2017/company/president.html" class="hover"><img src="/recruit/2017/common/img/company-menu1.png" alt="社長メッセージ"></a></li>
			<li><a href="/recruit/2017/company/train/index.html" class="hover"><img src="/recruit/2017/common/img/company-menu3.png" alt="育成・研修体系　キャリアプラン"></a></li>
			<li><a href="/recruit/2017/company/change/index.html" class="hover"><img src="/recruit/2017/common/img/company-menu4.png" alt="情報通信は世の中を変える"></a></li>
		</ul>
		<ul class="sub-menu cfix">
			<li><a href="/recruit/2017/recruit/charge.html" class="hover"><img src="/recruit/2017/common/img/recruit-menu1.png" alt="採用情報　採用責任者の声"></a></li>
			<li><a href="/recruit/2017/recruit/point.html" class="hover"><img src="/recruit/2017/common/img/recruit-menu2.png" alt="募集要項"></a></li>
			<li><a href="/recruit/2017/recruit/schedule.html" class="hover"><img src="/recruit/2017/common/img/recruit-menu3.png" alt="新卒採用スケジュール"></a></li>
			<li><a href="/recruit/2017/recruit/public.html" class="hover"><img src="/recruit/2017/common/img/recruit-menu4.png" alt="福利厚生＆休暇等"></a></li>
			<li><a href="/recruit/2017/recruit/faq.html" class="hover"><img src="/recruit/2017/common/img/recruit-menu5.png" alt="FAQ"></a></li>
		</ul>
	</nav>