	<header>
		<h1><a href="/recruit/2017/sp/"><img src="/recruit/2017/sp/common/img/h1.jpg" alt="NTT-ME 2017 RECRUITING SITE"></a></h1>
		<div id="gnavi-btn"><img src="/recruit/2017/sp/common/img/gnavi-btn.jpg"></div>
	</header>
	<div id="gnavi">
		<nav>
			<ul>
				<li><a href="https://job.axol.jp/16/s/nttme/m/" target="_blank"><span><img src="/recruit/2017/sp/common/img/gnavi1.jpg" alt="マイページ"></span></a></li>
				<li><a href="/recruit/2017/pre/" target="_blank"><span><img src="/recruit/2017/sp/common/img/gnavi2.jpg" alt="エントリー"></span></a></li>
				<li><a href="/recruit/2017/sp/"><span><img src="/recruit/2017/sp/common/img/gnavi3.jpg" alt="トップページ"></span></a></li>
				<li><a href="/recruit/2017/works/works.html" target="_blank"><span><img src="/recruit/2017/sp/common/img/gnavi4.jpg" alt="WORKS 仕事"></span></a></li>
				<li><a href="/recruit/2017/engineers/senior/index.html" target="_blank"><span><img src="/recruit/2017/sp/common/img/gnavi5.jpg" alt="ENGINEERS 社員"></span></a></li>
				<li>
					<a href="" class="toggle"><span><img src="/recruit/2017/sp/common/img/gnavi6.jpg" alt="COMPANY 会社"></span></a>
					<ul>
						<li><a href="/recruit/2017/sp/company/company.html"><span><img src="/recruit/2017/sp/common/img/gnavi6-2.jpg" alt="会社概要"></span></a></li>
						<li><a href="/recruit/2017/sp/company/president.html"><span><img src="/recruit/2017/sp/common/img/gnavi6-1.jpg" alt="社長メッセージ"></span></a></li>
						<li><a href="/recruit/2017/sp/company/train/index.html"><span><img src="/recruit/2017/sp/common/img/gnavi6-3.jpg" alt="育成・研修体系　キャリアプラン"></span></a></li>
						<!--<li><a href="/recruit/2017/sp/company/change/index.html"><span><img src="/recruit/2017/sp/common/img/gnavi6-4.jpg" alt="情報通信は世の中を変える"></span></a></li>-->
					</ul>
				</li>
				<li>
					<a href="" class="toggle"><span><img src="/recruit/2017/sp/common/img/gnavi7.jpg" alt="RECRUIT 採用"></span></a>
					<ul>
						<li><a href="/recruit/2017/sp/recruit/charge.html"><span><img src="/recruit/2017/sp/common/img/gnavi7-1.jpg" alt="採用情報 採用責任者の声"></span></a></li>
						<li><a href="/recruit/2017/sp/recruit/point.html"><span><img src="/recruit/2017/sp/common/img/gnavi7-2.jpg" alt="募集要項"></span></a></li>
						<li><a href="/recruit/2017/sp/recruit/schedule.html"><span><img src="/recruit/2017/sp/common/img/gnavi7-3.jpg" alt="新卒採用スケジュール"></span></a></li>
						<li><a href="/recruit/2017/sp/recruit/public.html"><span><img src="/recruit/2017/sp/common/img/gnavi7-4.jpg" alt="福利厚生＆休暇等"></span></a></li>
						<li><a href="/recruit/2017/sp/recruit/faq.html"><span><img src="/recruit/2017/sp/common/img/gnavi7-5.jpg" alt="FAQ"></span></a></li>
					</ul>
				</li>
				<li><a href="/recruit/2017/quiz/index.html" target="_blank"><span><img src="/recruit/2017/sp/common/img/gnavi8.jpg" alt="NTT-ME クイズ"></span></a></li>
			</ul>
		</nav>
	</div>